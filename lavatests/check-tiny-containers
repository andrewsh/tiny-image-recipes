#!/bin/sh

# Copyright © 2018 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -ue

LXC_NAME=${LXC_NAME:=tinytest}
OSPACK=""
TEMPLATE=""
ARCH=""
RELEASE=""
DATE=""
PARAMS=""
CUSTOM_TEST=""
TYPE=${TYPE:=rfs}

aa_namespace=${aa_namespace:=lxc-apertis-tiny}
imageshost=${imageshost:=images.apertis.org}

help(){
cat <<E_O_F
Usage:
$0 <--arch "amd64"> <--release 18.04> <--date 20180313.0> <--template /path/to/template>
$0 <-a "amd64"> <-r 18.04> <-d 20180313.0> <-t /path/to/template>
$0 <--ospack "http://url"> <--template /path/to/template>
$0 <-o "http://url"> <-t /path/to/template>
E_O_F
}

opts=$(getopt -o "o:t:a:r:d:p:c:" -l "ospack:,template:,arch:,release:,date:,params:,aa-namespace:,custom-test:" -- "$@")
eval set -- "$opts"

while [ $# -gt 0 ]; do
    case $1 in
        -o|--ospack) OSPACK="$2"; shift 2;;
        -t|--template) TEMPLATE="$2"; shift 2;;
        -a|--arch) ARCH="$2"; shift 2;;
        -r|--release) RELEASE="$2"; shift 2;;
        -d|--date) DATE="$2"; shift 2;;
        -p|--params) PARAMS="$2"; shift 2;;
        -c|--custom-test) CUSTOM_TEST="$2"; shift 2;;
        --aa-namespace) aa_namespace="$2"; shift 2;;
        --) shift;;
        *) help; exit 1;;
    esac
done

[ -z "$TEMPLATE" ] && help && exit 1
if [ ! -f "$TEMPLATE" ]; then
    echo "Template '$TEMPLATE' does not exist"
    exit 1
fi

TEMPLATE="$(readlink -f $TEMPLATE)" # LXC needs full path to template

if [ -z "$OSPACK" ]; then
    # Check if we able to construct ospack path
    [ -z "$ARCH" -o -z "$RELEASE" -o -z "$DATE" ] && help && exit 1
    OSPACK=https://${imageshost}/daily/experimental/${RELEASE}/${DATE}/${ARCH}/${TYPE}/isolation_${TYPE}-${RELEASE}-${ARCH}-${DATE}.tar.gz
fi


container_type="privileged"

# pass NAME [MESSAGE...]
pass () {
    echo "container-${container_type}-$1:" "pass"
}

# fail NAME [MESSAGE...]
fail () {
    echo "container-${container_type}-$1:" "fail"
    exit 1
}

# skip NAME
skip () {
    echo "container-${container_type}-$1:" "skip"
}

# For non-privileged container additional step and options are needed
unprivileged_opts=""
if [ "$(id -u)" != 0 ]; then
    LXC_NAME=${LXC_NAME}-$USER
    container_type="non_privileged"

    unprivileged_opts="--unprivileged --mapped-uid 100000 --mapped-gid 100000"

    mkdir -p ~/.config/lxc
    cat <<E_O_F > ~/.config/lxc/default.conf
lxc.id_map = u 0 100000 65536
lxc.id_map = g 0 100000 65536
E_O_F
fi

# Cleanup container if needed
lxc-ls --active -1 | grep -q $LXC_NAME && lxc-stop -k -n $LXC_NAME
lxc-ls -1 | grep -q $LXC_NAME && lxc-destroy -n $LXC_NAME

###############################
# Check ospack availability but give a chance to download it by lxc template
test_name=ospack_availability
if [ ${OSPACK%%://*} = "https" -o ${OSPACK%%://*} = "http" ]; then
    if wget --spider -q "$OSPACK"; then
        pass $test_name
    else
        fail $test_name
    fi
fi

# Change directory to home dir to resolve permission problems during downloads
cd

###############################
# check if userns is allowed
test_name=unshare_userns
if [ "$(id -u)" -ne 0 ]; then
    if [ "$(sysctl -n kernel.unprivileged_userns_clone)" = "0" ]; then
        echo "Please allow to use userns with:"
        echo "    sysctl -w kernel.unprivileged_userns_clone=1"
        fail $test_name
    else
        #Additional check
        unshare -U -- id -u >/dev/null && pass $test_name || fail $test_name
    fi
else
    skip $test_name
fi

###############################
# Check if apparmor rules could be loaded for container
# Need to have CAP_MAC_ADMIN capabilities so started as root only
# `mkdir /sys/kernel/security/apparmor/policy/namespaces/ns` it is not work in 3.14 (#T4693)
test_name=apparmor_ns
if [ "$(id -u)" -eq 0 ]; then
    rules="file, signal, unix, dbus, ptrace, mount, pivot_root, capability,"
    echo "profile :testns1:p1 { $rules }" | apparmor_parser -qr || fail $test_name
    if ! aa-exec -n testns1 -p p1 -- unshare -Ur true ; then
        fail $test_name
    fi
    # Check if profile is correctly aplied for container
    if [ "$(aa-exec -n testns1 -p p1 -- unshare -Ur cat /sys/kernel/security/apparmor/profiles )" = "p1 (enforce)" ] ; then
        pass $test_name
    else
        fail $test_name
    fi
else
    skip $test_name
fi

###############################
# Create container
test_name=create
if lxc-create -t "$TEMPLATE" --name $LXC_NAME -- --ospack "$OSPACK" --aa-namespace "$aa_namespace" $PARAMS $unprivileged_opts; then
    pass $test_name
else
    fail $test_name
fi

###############################
# Check if AppArmor namespace is created before lxc-start is called
# Namespace should be created outside of this script
test_name=aa_namespace
if [ ! -d "/sys/kernel/security/apparmor/policy/namespaces/$aa_namespace" ]; then
    echo "AppArmor namespace \"$aa_namespace\" is not available."
    fail $test_name
fi

###############################
# Check if container is able to run shell
test_name=start
if lxc-start -F --name $LXC_NAME -- /bin/true; then
    pass $test_name
else
    fail $test_name
fi

###############################
# Check if container have an empty network (loopback only) for non-privileged container
# And the same network interfaces as the host has for privileged container
test_name=networking

ifaces=`lxc-start -F --name $LXC_NAME -- sh -c "ls -1 /sys/class/net" | grep -vw '^lo$' | wc -l`
expected_ifaces="0" # for non-privileged container
if [ -z "$unprivileged_opts" ]; then
    # for privileged container
    expected_ifaces="`ls -1 /sys/class/net | grep -vw '^lo$' | wc -l`"
fi

if [ "$ifaces" -eq "$expected_ifaces" ]; then
    pass $test_name
else
    fail $test_name
fi

lxc_stop_if_running() {
    if ! lxc-stop -k --name $1
    then
        # 0 means success
        # 1 means error
        # 2 means it wasn't running
        [ $? != 1 ]
    else
        exit 0
    fi

}

###############################
# Start the OS in container with a background mode
test_name=os_start
if lxc-start --name $LXC_NAME; then
    pass $test_name
    trap "lxc_stop_if_running $LXC_NAME" EXIT
else
    fail $test_name
fi

###############################
# Check if container is in "running" state
test_name=os_running
if lxc-wait -t 30 -s RUNNING --name $LXC_NAME; then
    pass $test_name
else
    fail $test_name
fi

###############################
# Check if systemd started correctly
if [ -z "$CUSTOM_TEST" ] ; then
    test_name=os_boot

    tries=30
    while true; do
        state=`lxc-attach --name $LXC_NAME -- systemctl is-system-running || :`

        case "$state" in
        running)
            pass $test_name
            break
            ;;
        degraded)
            echo "Some services are in failed state:"
            lxc-attach --name $LXC_NAME -- systemctl --state=failed --no-legend
            fail $test_name
            ;;
        *)
            tries=`expr $tries - 1`
            [ $tries -le 0 ] && fail $test_name || :
            sleep 1
            ;;
        esac
    done

###############################
# Custom test
else
    test_name=$(basename "$CUSTOM_TEST")

    if cat "$CUSTOM_TEST" | lxc-attach --name $LXC_NAME sh ; then
        pass $test_name
    else
        fail $test_name
    fi
fi

test_name=system-dbus
if lxc-attach --name $LXC_NAME -- dbus-send --system --print-reply \
   --dest=org.freedesktop.systemd1  \
   /org/freedesktop/systemd1 org.freedesktop.DBus.Properties.Get \
   string:'org.freedesktop.systemd1.Manager' string:'Version'
then
    pass $test_name
else
    fail $test_name
fi

test_name=systemd-tmpfiles
if lxc-attach --name $LXC_NAME -- systemctl is-active systemd-tmpfiles-setup.service
then
    pass $test_name
else
    fail $test_name
fi
lxc-attach --name $LXC_NAME -- systemctl status systemd-tmpfiles-setup.service

test_name=graceful_shutdown
if lxc-attach --name $LXC_NAME -- systemctl halt
then
    if lxc-wait -t 30 -s STOPPED --name $LXC_NAME; then
        echo The container has been cleanly shut down.
        pass $test_name
    else
        fail $test_name
    fi
else
    fail $test_name
fi

exit 0

#!/bin/sh
set -eu

DEBUGOUT="/dev/null" #"&1" to get output in console

MUSTFAIL()
{
	if $* >$DEBUGOUT 2>&1; then
		echo $* unexpected pass.
		exit 1
	else
		echo $* failed as expected.
	fi
}

MUSTPASS()
{
	if $* >$DEBUGOUT 2>&1; then
		echo $* pass as expected.
	else
		echo $* unexpected failure.
		exit 1
	fi
}

TESTING ()
{
	echo "#"
	echo "# $*"
	echo "#"
}

TESTING "Sanity checks"
BINARIES="ls touch mkdir grep head cat cp mknod"
for binary in $BINARIES aa-enabled; do
	MUSTPASS test "$binary" = "$(basename $(which $binary))"
done
for binary in $BINARIES ; do
	MUSTPASS $binary --version
done
MUSTPASS test `aa-enabled` = "Yes"

TESTING "Accessing sensitive informations in /sys"
MUSTPASS ls /sys/kernel/security/
MUSTPASS ls /sys/kernel/security/apparmor/
MUSTPASS ls /sys/kernel/security/apparmor/policy/namespaces/
MUSTFAIL ls /sys/kernel/debug

TESTING "Setting kernel parameters by writing to /sys"
MUSTPASS ls /sys/class/gpio/
MUSTFAIL touch /sys/class/gpio/export

TESTING "Accessing memory outside of its realm"
MUSTPASS ls /proc/
MUSTFAIL head /proc/kmem
MUSTFAIL head /proc/mem
MUSTFAIL head /dev/mem
MUSTFAIL head /proc/kcore

TESTING "Writing to the base container rootfs except /var and /run"
for path in /var /run /tmp ; do
	MUSTPASS touch $path
	MUSTPASS touch $path/aa-test-file
	MUSTPASS mkdir $path/aa-test-folder
done
ls / | grep -v -E "var|run|tmp" | while read path; do
	MUSTFAIL touch $path/aa-test-file
	MUSTFAIL mkdir $path/aa-test-folder
#	MUSTFAIL touch $path
done

# reading and writing outside of the rootfs
# From the container, this is controlled by the container filesystem
# To test it would require to identify a host file from the container
# And make sure it cannot be changed

TESTING "Accessing devices outside of its realm mounting devpts"
MUSTPASS ls -l /dev
MUSTPASS ls -l /dev/console
MUSTFAIL mknod /dev/console.fake c 5 1
MUSTPASS ls -l /dev
MUSTFAIL mknod /dev/sda1.fake b 8 1
MUSTPASS ls -l /dev
MUSTFAIL cp /dev/null /dev/ptmx
MUSTFAIL cp /dev/null /dev/pts/0

exit 0
